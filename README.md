# Binar-FEJS-Challenge-5

By Andhika Dian Pratama

## How to start the application ?

1. Go to the directory backend with terminal and start with "npm start".
2. Then go to the directory server with terminal and start with "npm start".
3. Then go to the directory movie-app with terminal and start with "npm start".
4. Finally go to the browser and open this url "http://localhost:3000"
