//oAuth strategy
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const FacebookStrategy = require("passport-facebook").Strategy;
const GithubStrategy = require("passport-github2").Strategy;

//Client id and secret of google
const GOOGLE_CLIENT_ID =
  "567058461433-a93oblif7s5q9kh1724bnmhrba08nncp.apps.googleusercontent.com";
const GOOGLE_CLIENT_SECRET = "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";
const passport = require("passport");

//Client id and secret of github
const GITHUB_CLIENT_ID = "Iv1.903035623e571dc3";
const GITHUB_CLIENT_SECRET = "f1f40d94f60dfceb28e85d222f8d32650e34587e";

//Client id and secret of facebook
const FACEBOOK_APP_ID = "192266476535306";
const FACEBOOK_APP_SECRET = "8ddf1e1d34cc0bce79929fe2f182c6ac";

//Config with google strategy
passport.use(
  new GoogleStrategy(
    {
      clientID: GOOGLE_CLIENT_ID,
      clientSecret: GOOGLE_CLIENT_SECRET,
      callbackURL: "/auth/google/callback",
    },
    function (accessToken, refreshToken, profile, done) {
      done(null, profile);
    }
  )
);

//Config with github strategy
passport.use(
  new GithubStrategy(
    {
      clientID: GITHUB_CLIENT_ID,
      clientSecret: GITHUB_CLIENT_SECRET,
      callbackURL: "/auth/github/callback",
    },
    function (accessToken, refreshToken, profile, done) {
      done(null, profile);
    }
  )
);

//Config with facebook strategy
passport.use(
  new FacebookStrategy(
    {
      clientID: FACEBOOK_APP_ID,
      clientSecret: FACEBOOK_APP_SECRET,
      callbackURL: "/auth/facebook/callback",
    },
    function (accessToken, refreshToken, profile, done) {
      done(null, profile);
    }
  )
);

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});
