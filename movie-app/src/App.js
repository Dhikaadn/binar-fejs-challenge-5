//Import
import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { getMovieList, searchMovie } from "./api";
import { useEffect, useState } from "react";
import Home from "./component/Home";
import { Navigate } from "react-router-dom";
import { Details } from "./component/Details";
import { Login } from "./component/Login";
import { Navbar } from "./component/Navbar";
import { Register } from "./component/Register";

const App = () => {
  //Authentication for token with JWT
  const user2 = localStorage.getItem("token");
  //Initial state of user status
  const [user, setUser] = useState(null);
  //Initial state of resultTitle
  const [resultTitle, setResultTitle] = useState("Popular Movies");
  //Initial state of popularMovies
  const [popularMovies, setPopularMovies] = useState([]);

  useEffect(() => {
    getMovieList().then((result) => {
      setPopularMovies(result);
    });
  }, []);

  //Set the user status
  useEffect(() => {
    const getUser = () => {
      fetch("http://localhost:5000/auth/login/success", {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "Access-Control-Allow-Credentials": true,
        },
      })
        .then((response) => {
          if (response.status === 200) return response.json();
          throw new Error("authentication has been failed!");
        })
        .then((resObject) => {
          setUser(resObject.user);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getUser();
  }, []);

  //Functional of search
  const search = async (q) => {
    if (q.length > 3) {
      const query = await searchMovie(q);
      setPopularMovies(query.results);
      setResultTitle(`Search Result "${q}"`);
    } else if (q.length < 3) {
      setResultTitle("Popular Movies");
    }
  };
  return (
    <div className="App">
      {/* Routing */}
      <Router>
        {/* Viewing navbar */}
        <Navbar onSearch={search} user={user} user2={user2} />
        <Routes>
          <Route
            path="/"
            element={
              <Home
                onListMovie={popularMovies}
                onResultTitle={resultTitle}
                user={user}
                user2={user2}
              />
            }
          />
          <Route
            path="/details/:id"
            element={user || user2 ? <Details /> : <Navigate to="/login" />}
          />
          <Route
            path="/login"
            element={user || user2 ? <Navigate to="/" /> : <Login />}
          />
          <Route path="/register" element={<Register />} />
        </Routes>
      </Router>
    </div>
  );
};

export default App;
