//Import axios
import axios from "axios";

//Declare API key and Base URL
const apiKey = process.env.REACT_APP_APIKEY;
const baseUrl = process.env.REACT_APP_BASEURL;

//Endpoint API to GET all popular movies
export const getMovieList = async () => {
  const movie = await axios.get(
    `${baseUrl}/movie/popular?page=1&api_key=${apiKey}`
  );
  return movie.data.results;
};

//Endpoint API to GET movies based on query search parameters
export const searchMovie = async (q) => {
  const search = await axios.get(
    `${baseUrl}/search/movie?page=1&api_key=${apiKey}&query=${q}`
  );
  return search.data;
};

//Endpoint API to GET movies based on id
export const detailsMovie = async (id) => {
  const details = await axios.get(`${baseUrl}/movie/${id}?api_key=${apiKey}`);
  return details.data;
};
