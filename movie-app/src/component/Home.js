//Import style and component
import "../App.css";
import { Header } from "./Header";
import { TheFooter } from "./TheFooter";
import { AiFillPlayCircle } from "react-icons/ai";
import { ResultTitle } from "./ResultTitle";
import { Link } from "react-router-dom";
import { MdOutlineStarRate } from "react-icons/md";

//Function App
const Home = ({ onListMovie, onResultTitle, user, user2 }) => {
  //View list of movies
  const PopularMovieList = () => {
    return onListMovie.map((movie, i) => {
      return (
        //Link to details component
        <Link to={`details/${movie.id}`} style={{ textDecoration: "none" }}>
          <div className="Movie-wrapper" key={i}>
            <div className="Movie-title">
              <p>{movie.title}</p>
            </div>
            <img
              className="Movie-image"
              src={`${process.env.REACT_APP_BASEIMGURL}/${movie.poster_path}`}
              width="300"
            />
            <div className="Movie-rate">
              {`Rate : ${movie.vote_average}`}
              <MdOutlineStarRate
                className=""
                style={{ fontSize: "25px", color: "yellow" }}
              />
            </div>
            <div className="container-watch-card">
              <button className="bt-watch-card">
                <div className="icon-play">
                  <AiFillPlayCircle />
                </div>
                <p>Watch Trailer</p>
              </button>
            </div>
          </div>
        </Link>
      );
    });
  };

  return (
    //View of all components
    <div className="App">
      <Header />
      {/* Conditional if user status true or false */}
      {user || user2 ? (
        <h2 className="welcome-text">Welcome to movie list :)</h2>
      ) : (
        <h2 className="welcome-text">
          Hi Guest, please login first to see the details movie :(
        </h2>
      )}
      <ResultTitle onResult={onResultTitle} />
      <div className="Movie-container">
        <PopularMovieList />
      </div>
      <TheFooter />
    </div>
  );
};

export default Home;
