//Import style and component
import React from "react";
import "../App.css";
import axios from "axios";
import { useState } from "react";
import google_img from "../img/google.png";
import facebook_img from "../img/facebook.png";
import github_img from "../img/github.png";
import { AiOutlineMail } from "react-icons/ai";
import { AiFillEyeInvisible } from "react-icons/ai";

export const Login = () => {
  //Inintial state of email and password
  const [data, setData] = useState({ email: "", password: "" });
  const [error, setError] = useState("");

  const handleChange = ({ currentTarget: input }) => {
    setData({ ...data, [input.name]: input.value });
  };

  //Handle submit of user status with JWT login
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const url = "http://localhost:8080/api/auth";
      const { data: res } = await axios.post(url, data);
      localStorage.setItem("token", res.data);
      window.location = "/";
    } catch (error) {
      if (
        error.response &&
        error.response.status >= 400 &&
        error.response.status <= 500
      ) {
        setError(error.response.data.message);
      }
    }
  };

  //Login with google
  const google = () => {
    window.open("http://localhost:5000/auth/google", "_self");
  };
  //Login with github
  const github = () => {
    window.open("http://localhost:5000/auth/github", "_self");
  };
  //Login with facebook
  const facebook = () => {
    window.open("http://localhost:5000/auth/facebook", "_self");
  };
  return (
    //Viewing the component
    <div className="Login">
      <div className="cart-login">
        {/* Login with JWT */}
        <form className="form-login" onSubmit={handleSubmit}>
          <h2 className="login-text">Log in to your account</h2>
          <label className="login-text">Email</label>
          <div className="container-input">
            <input
              name="email"
              type="email"
              placeholder="Email Address"
              className="input-field"
              onChange={handleChange}
              value={data.email}
            />
            <AiOutlineMail className="me-2" style={{ fontSize: "25px" }} />
          </div>
          <label className="login-text">Password</label>
          <div className="container-input">
            <input
              name="password"
              type="password"
              placeholder="Password"
              className="input-field"
              onChange={handleChange}
              value={data.password}
            />
            <AiFillEyeInvisible className="me-2" style={{ fontSize: "25px" }} />
          </div>
          {error && <div>{error}</div>}
          <button className="bt-login-submit">Login</button>
        </form>
        <div>
          <p>Or</p>
          <p>Login with</p>
        </div>
        <div className="cart-social">
          {/* Button of google */}
          <div onClick={google}>
            <img src={google_img} />
          </div>
          {/* Button of facebook */}
          <div onClick={facebook}>
            <img src={facebook_img} />
          </div>
          {/* Button of github */}
          <div onClick={github}>
            <img src={github_img} />
          </div>
        </div>
      </div>
    </div>
  );
};
