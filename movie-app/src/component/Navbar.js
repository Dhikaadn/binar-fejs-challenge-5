//Import style and components
import "../App.css";
import React from "react";
import { AiOutlineSearch } from "react-icons/ai";
import { Link } from "react-router-dom";

export const Navbar = ({ onSearch, user, user2 }) => {
  //Logout process
  const logout = () => {
    localStorage.removeItem("token");
    window.location.reload();
    window.open("http://localhost:5000/auth/logout", "_self");
  };
  return (
    //View of the navbar
    <nav className="navbar-top">
      <h1 className="text-title">Movielist</h1>
      {/* Field of search */}
      <div className="container-search">
        <input
          placeholder="Cari film"
          className="Movie-search"
          onChange={({ target }) => onSearch(target.value)}
        />
        <AiOutlineSearch className="mt-2 me-3" style={{ fontSize: "35px" }} />
      </div>
      {/* Conditional of button login, register, and logout */}
      {user || user2 ? (
        <div className="btn-container">
          <button className="bt-login" onClick={logout}>
            Logout
          </button>
        </div>
      ) : (
        <div className="btn-container">
          <Link to="/login">
            <button className="bt-login">Login</button>
          </Link>
          <Link to="/register">
            <button className="bt-register">Register</button>
          </Link>
        </div>
      )}
    </nav>
  );
};
