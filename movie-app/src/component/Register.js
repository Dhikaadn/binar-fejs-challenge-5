//Import components
import React from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useState } from "react";
import google_img from "../img/google.png";
import facebook_img from "../img/facebook.png";
import github_img from "../img/github.png";
import { AiOutlineMail } from "react-icons/ai";
import { AiOutlineUser } from "react-icons/ai";
import { AiFillEyeInvisible } from "react-icons/ai";

export const Register = () => {
  //Register with JWT
  const [data, setData] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
  });
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const handleChange = ({ currentTarget: input }) => {
    setData({ ...data, [input.name]: input.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const url = "http://localhost:8080/api/users";
      const { data: res } = await axios.post(url, data);
      navigate("/login");
      console.log(res.message);
    } catch (error) {
      if (
        error.response &&
        error.response.status >= 400 &&
        error.response.status <= 500
      ) {
        setError(error.response.data.message);
      }
    }
  };
  //Register with google
  const google = () => {
    window.open("http://localhost:5000/auth/google", "_self");
  };
  //Register with github
  const github = () => {
    window.open("http://localhost:5000/auth/github", "_self");
  };
  //Register with facebook
  const facebook = () => {
    window.open("http://localhost:5000/auth/facebook", "_self");
  };
  return (
    //Viewing of Register component
    <div className="Register">
      <div className="cart-register">
        <form className="form-register" onSubmit={handleSubmit}>
          <h2 className="register-text">Create Account</h2>
          <label className="register-text">First Name</label>
          <div className="container-input">
            <input
              name="firstName"
              placeholder="First name"
              className="input-field"
              onChange={handleChange}
              value={data.firstName}
              required
            />
            <AiOutlineUser className="me-2" style={{ fontSize: "25px" }} />
          </div>
          <label className="register-text">Last Name</label>
          <div className="container-input">
            <input
              name="lastName"
              placeholder="Last name"
              className="input-field"
              onChange={handleChange}
              value={data.lastName}
              required
            />
            <AiOutlineUser className="me-2" style={{ fontSize: "25px" }} />
          </div>
          <label className="register-text">Email Address</label>
          <div className="container-input">
            <input
              name="email"
              type="email"
              placeholder="Email address"
              className="input-field"
              onChange={handleChange}
              value={data.email}
              required
            />
            <AiOutlineMail className="me-2" style={{ fontSize: "25px" }} />
          </div>
          <label className="register-text">Password</label>
          <div className="container-input">
            <input
              name="password"
              type="password"
              placeholder="Password"
              className="input-field"
              onChange={handleChange}
              value={data.password}
              required
            />
            <AiFillEyeInvisible className="me-2" style={{ fontSize: "25px" }} />
          </div>
          {error && <div>{error}</div>}
          <button className="bt-register-submit">Register now</button>
        </form>
        <div>
          <p>Or</p>
          <p>Register with</p>
        </div>
        <div className="cart-social">
          {/* Button of google */}
          <div onClick={google}>
            <img src={google_img} />
          </div>
          {/* Button of facebook */}
          <div onClick={facebook}>
            <img src={facebook_img} />
          </div>
          {/* Button of github */}
          <div onClick={github}>
            <img src={github_img} />
          </div>
        </div>
      </div>
    </div>
  );
};
