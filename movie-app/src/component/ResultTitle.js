//Import
import "../App.css";
import React from "react";

export const ResultTitle = ({ onResult }) => {
  return (
    //View of the search result
    <div className="title-popular-movie">
      <h2>{onResult}</h2>
    </div>
  );
};
